import axios from 'axios'
//定义fetch函数，config为配置
export function fetch(config){
  //返回promise对象
  return new Promise((resolve,reject) =>{
    //创建axios实例，把基本的配置放进去
    const instance = axios.create({
      //定义请求文件类型
      headers:{
        'Content-Type': 'application/json',
      },
      // 请求超时
      timeout: 5000,
      //定义请求根目录
      baseURL: 'http://kf.wuyoufang.cn:81/'
    });
    //请求成功后执行的函数
    instance(config).then(res =>{
      // console.log(res);
      resolve(res);
      //失败后执行的函数
    }).catch(err => {
      // console.log(err);
      reject(err);
    })
  });
}

// 登录
export function Login(data) {
  return fetch({
    url: 'login',
    method: 'post',
    data: data,
  })
}
